from tkinter import *
from itertools import cycle
from copy import copy

class Calculator:

	def __init__(self):
		'''Set the window for app'''
		self.root = Tk()
		self.root.title('My Tkinter Calculator')
		self.root.geometry("360x240")

		'''Set the field to enter data'''
		self.Textbox = Entry(self.root, width=52, bd=4)
		self.Textbox.grid(row=0, column=0, columnspan=4)

		'''Create buttons for digits and operations'''
		self.Button1 = Button(self.root, text=1, width=10, command=lambda: self.digit_button(1)).grid(row=1, column=0)
		self.Button2 = Button(self.root, text=2, width=10, command=lambda: self.digit_button(2)).grid(row=1, column=1)
		self.Button3 = Button(self.root, text=3, width=10, command=lambda: self.digit_button(3)).grid(row=1, column=2)
		self.Button4 = Button(self.root, text=4, width=10, command=lambda: self.digit_button(4)).grid(row=2, column=0)
		self.Button5 = Button(self.root, text=5, width=10, command=lambda: self.digit_button(5)).grid(row=2, column=1)
		self.Button6 = Button(self.root, text=6, width=10, command=lambda: self.digit_button(6)).grid(row=2, column=2)
		self.Button7 = Button(self.root, text=7, width=10, command=lambda: self.digit_button(7)).grid(row=3, column=0)
		self.Button8 = Button(self.root, text=8, width=10, command=lambda: self.digit_button(8)).grid(row=3, column=1)
		self.Button9 = Button(self.root, text=9, width=10, command=lambda: self.digit_button(9)).grid(row=3, column=2)
		self.Button0 = Button(self.root, text=0, width=10, command=lambda: self.digit_button(0)).grid(row=4, column=1)
		#Operators and others
		self.Button_dot = Button(self.root, text='.', width=10, command=lambda: self.digit_button('.')).grid(row=4, column=0)
		self.Button_eq = Button(self.root, text='=', width=10, command=self.eq_button).grid(row=4, column=2)
		self.Button_add = Button(self.root, text='+', width=10, command=self.add_button).grid(row=1, column=3)
		'''self.Button_sub = Button(self.root, text='-', width=10, command=self.dot_button).grid(row=2, column=3)
		self.Button_mul = Button(self.root, text='*', width=10, command=self.dot_button).grid(row=3, column=3)
		self.Button_div = Button(self.root, text='/', width=10, command=self.dot_button).grid(row=4, column=3)'''

		'''  '''
		self.first_num = 0
		self.second_num = 0
		self.first_num_copy = 0 #to save first number when it will be deleted from Textbox
		self.operator = ''

	def digit_button(self, text):
		#Textbox.delete(0, END)
		self.Textbox.insert(END, str(text))

	def add_button(self):
		self.operator = 'addition'
		self.first_num = self.Textbox.get()
		self.first_num_copy = float(self.first_num)
		self.Textbox.delete(0, END)	

	def eq_button(self):
		self.second_num = self.Textbox.get()
		self.Textbox.delete(0, END)
		self.Textbox.insert(0, self.first_num_copy + float(self.second_num))


	def run_calculator(self):
		self.root.mainloop()

if __name__ == "__main__":
	Calculator().run_calculator()
